const { GraphQLClient } = require('graphql-request')
const fs = require('fs-extra')
const conf = require('rc')('payback')
// Client Auth setup
const client = new GraphQLClient('https://dinero.cirruscreative.host/', {
  headers: {
    Authorization: `Bearer ${conf.token}`
  }
})

// Mutations
const createProjectMutation = `mutation createProject($name: String!, $dependencies: Json!) {
  createProject(name: $name, dependencies: $dependencies) {
    id
    name
  }
}`

const updateProjectMutation = `mutation updateProject($projectId: ID!, dependencies: Json! {
  updateProject(where: {id: $projectId}, data: { dependencies: $dependencies }) {
    id
  }
}`

const deleteProjectMutation = `mutation deleteProject($projectId: ID!) {
  deleteProject(projectId: $projectId) {
    id
  }
}`
// Functions
const project = {
  async createProject (projectName) {
    // Get local package.json
    const packageJson = await fs.readJson(`${process.cwd()}/package.json`).then(packageObj => packageObj).catch(err => err)
    // Add project name & (dev)dependencies to an object
    const variables = {
      name: projectName,
      dependencies: { ...packageJson.dependencies, ...packageJson.devDependencies }
    }
    // Create Project in backend
    return client.request(createProjectMutation, variables).then(data => data.createProject).catch(err => {throw new Error(err)})
  },
  async updateProject (projectId) {
    // Get local package.json
    const packageJson = await fs.readJson(`${process.cwd()}/package.json`).then(packageObj => packageObj).catch(err => err)
    // Add project name & (dev)dependencies to an object
    const variables = {
      projectId,
      dependencies: { ...packageJson.dependencies, ...packageJson.devDependencies }
    }
    // Update Project in backend
    return client.request(updateProjectMutation, variables).then(data => data.updateProject).catch(err => {throw new Error(err)})
  },
  async deleteProject (projectId) {
    return client.request(deleteProjectMutation, projectId).then(data => data.createProject).catch(err => {throw new Error(err)})
  }
}

module.exports = { project }