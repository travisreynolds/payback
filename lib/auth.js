const { request } = require('graphql-request')
const fs = require('fs-extra')
const homedir = require('os').homedir()
const filepath = `${homedir}/.paybackrc`
const ora = require('ora')

// Mutations
const loginMutation = `mutation login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    token
  }
}`

const signupMutation = `mutation signup($email: String!, $password: String!) {
  signup(email: $email, password: $password) {
    token
  }
}`

// Functions
const auth = {
  async login (loginDetails) {
    const spinner = ora('Logging in...').start()
    const token = await request('https://dinero.cirruscreative.host/', loginMutation, loginDetails).then(({ login }) => login.token).catch(error => {throw new Error(error)})
    const fileContents = { "token": token }
    const writeRc = await fs.writeJson(filepath, fileContents).then(() => `Logged In!`).catch(err => console.error(err))
    spinner.stop().clear()
    return writeRc
  },
  async signup (signupDetails) {
    const spinner = ora('Registering...').start()
    const token = await request('https://dinero.cirruscreative.host/', signupMutation, signupDetails).then(({ signup }) => signup.token).catch(error => {throw new Error(error)})
    const fileContents = { "token": token }
    const writeRc = await fs.writeJson(filepath, fileContents).then(() => `Registered!`).catch(err => console.error(err))
    spinner.stop().clear()
    return writeRc
  }
}

module.exports = { auth }
